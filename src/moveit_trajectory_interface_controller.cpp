/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Ioan A. Sucan
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Ioan A. Sucan nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Lucas Joseph */

#include <moveit_simple_controller_manager_extended/moveit_trajectory_interface_controller.h>

namespace moveit_simple_controller_manager_extended
{

  bool MoveItTrajectoryInterfaceControllerHandle::sendTrajectory(const moveit_msgs::RobotTrajectory& robot_trajectory)
  {
    if (!controller_action_client_)
      return false;

    if (done_)
      ROS_DEBUG_STREAM("sending trajectory to " << name_);
    else
      ROS_DEBUG_STREAM("sending continuation for the currently executed trajectory to " << name_);

    control_msgs::JointTrajectoryGoal goal;
    goal.trajectory = robot_trajectory.joint_trajectory;

    controller_action_client_->sendGoal(goal,
                                      std::bind(&MoveItTrajectoryInterfaceControllerHandle::controllerDoneCallback, this,
                                                std::placeholders::_1, std::placeholders::_2),
                                      std::bind(&MoveItTrajectoryInterfaceControllerHandle::controllerActiveCallback, this),
                                      std::bind(&MoveItTrajectoryInterfaceControllerHandle::controllerFeedbackCallback,
                                                this, std::placeholders::_1));
    done_ = false;
    last_exec_ = moveit_controller_manager::ExecutionStatus::RUNNING;

    return true;
  }

  void MoveItTrajectoryInterfaceControllerHandle::controllerActiveCallback()
  {
    ROS_DEBUG_STREAM("Started execution");
  }

  void MoveItTrajectoryInterfaceControllerHandle::controllerDoneCallback(
    const actionlib::SimpleClientGoalState& state, const control_msgs::JointTrajectoryResultConstPtr& result)
  {
    // Output custom error message for FollowJointTrajectoryResult if necessary
    if (!result)
      ROS_DEBUG_STREAM("Controller '" << name_ << "' done, no result returned");
    else 
      ROS_DEBUG_STREAM("Controller '" << name_ << "' done");
    finishControllerExecution(state);
  }

  void MoveItTrajectoryInterfaceControllerHandle::controllerFeedbackCallback(
    const control_msgs::JointTrajectoryFeedbackConstPtr& /* feedback */)
  {
  }
}  // end namespace moveit_simple_controller_manager_extended