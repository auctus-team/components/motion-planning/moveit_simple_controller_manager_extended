# moveit_simple_controller_manager_extended

This package is a fork of [moveit_simple_controller_manager](https://github.com/ros-planning/moveit/tree/master/moveit_plugins/moveit_simple_controller_manager) that extends the available controller to add Auctus [moveit_trajectory_interface](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface).

To be able to run this controller manager, several files must be added/edited from panda_moveit_config. A working configuration is available [here](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_moveit_config).